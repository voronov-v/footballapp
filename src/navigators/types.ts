import { StackScreenProps } from '@react-navigation/stack';

export type RootStackParamList = {
  TEAMS_SCREEN: undefined;
  TEAM_INFO_SCREEN: { teamId: number };
};

export type RootStackScreenProps<T extends keyof RootStackParamList> = StackScreenProps<RootStackParamList, T>;
