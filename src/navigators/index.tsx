import React from 'react';
import 'react-native-gesture-handler';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { RootStackParamList } from '@root/navigators/types';
import { styles } from './styles';
import { TeamsScreen } from '@root/screens/TeamsScreen';
import { TeamInfoScreen } from '@root/screens/TeamInfoScreen';
import { TEAM_INFO_SCREEN, TEAMS_SCREEN } from '@root/general/consts/screenNames';
import { strings } from '@root/general/strings';

const RootStack = createStackNavigator<RootStackParamList>();

export const RootNavigator = () => {
  return (
    <NavigationContainer>
      <RootStack.Navigator
        screenOptions={{
          headerTitleAlign: 'center',
          headerStyle: styles.header,
          headerTitleStyle: styles.title,
        }}
      >
        <RootStack.Screen
          name={TEAMS_SCREEN}
          component={TeamsScreen}
          options={{ title: strings.teamsScreen.screenTitle }}
        />
        <RootStack.Screen
          name={TEAM_INFO_SCREEN}
          component={TeamInfoScreen}
          options={{ title: strings.teamInfoScreen.screenTitle }}
        />
      </RootStack.Navigator>
    </NavigationContainer>
  );
};
