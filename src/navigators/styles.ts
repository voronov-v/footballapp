import { Dimensions, StyleSheet } from 'react-native';
import { colors } from '@root/general/colors';

export const styles = StyleSheet.create({
  header: {
    backgroundColor: colors.white,
  },
  title: {
    fontSize: 17,
    lineHeight: 22,
    textTransform: 'uppercase',
    maxWidth: Dimensions.get('window').width / 1.5,
  },
});
