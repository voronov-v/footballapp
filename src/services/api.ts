import apisauce from 'apisauce';
import { BASE_URL, GENERAL_HEADERS } from '@root/general/consts';
import { ScheduleFilter } from '@root/general/types';

export type Api = ReturnType<typeof create>;

export const create = () => {
  const api = apisauce.create({
    baseURL: BASE_URL,
    timeout: 3000,
    headers: GENERAL_HEADERS,
  });

  const fetchTeamsRequest = () => {
    return api.get('teams');
  };

  const fetchTeamInfoRequest = (teamId: number) => {
    return api.get(`teams/${teamId}`);
  };

  const fetchTeamSchedule = (teamId: number, filter?: ScheduleFilter) => {
    const dateFrom = filter?.dateFrom ?? '';
    const limit = filter?.limit ?? '';

    return api.get(`teams/${teamId}/matches`, {
      'date-from': dateFrom,
      limit,
    });
  };

  return {
    fetchTeamsRequest,
    fetchTeamInfoRequest,
    fetchTeamSchedule,
  };
};
