import { IActionType } from '@root/redux/types';
import { Team } from '@root/general/types';
import { FETCH_TEAMS_SUCCEED } from '@root/general/consts/actions';

export type TeamsReducer = {
  teamsList: Array<Team>;
};

const INITIAL_STATE: TeamsReducer = {
  teamsList: [],
};

export const teamsReducer = (state = INITIAL_STATE, action: IActionType) => {
  switch (action.type) {
    case FETCH_TEAMS_SUCCEED:
      return { ...state, teamsList: [...action.payload] };
    default:
      return state;
  }
};
