import { combineReducers } from 'redux';
import { teamsReducer } from '@root/redux/reducers/teamsReducer';

export const rootReducer = combineReducers({
  teams: teamsReducer,
});
