import {
  CALL_COMMON_ERROR_HANDLER,
  FETCH_TEAM_INFO_REQUEST,
  FETCH_TEAM_SCHEDULE_REQUEST,
  FETCH_TEAMS_REQUEST,
  SHOW_TOAST,
} from '@root/general/consts/actions';
import { Match, ScheduleFilter, Team } from '@root/general/types';
import { ApiErrorResponse } from 'apisauce';
import { ToastTypes } from '@root/general/enums';

export type FetchTeamsPayload = {
  callback?: (isSuccess: boolean) => void;
};

export type FetchTeamsAction = {
  type: typeof FETCH_TEAMS_REQUEST;
  payload: FetchTeamsPayload;
};

export type FetchTeamInfoPayload = {
  teamId: number;
  callback?: (isSuccess: boolean, teamInfo?: Team) => void;
};

export type FetchTeamInfoAction = {
  type: typeof FETCH_TEAM_INFO_REQUEST;
  payload: FetchTeamInfoPayload;
};

export type FetchTeamSchedulePayload = {
  teamId: number;
  filter?: ScheduleFilter;
  callback?: (isSuccess: boolean, teamSchedule?: Array<Match>) => void;
};

export type FetchTeamScheduleAction = {
  type: typeof FETCH_TEAM_SCHEDULE_REQUEST;
  payload: FetchTeamSchedulePayload;
};

export type CommonErrorHandlerAction = {
  type: typeof CALL_COMMON_ERROR_HANDLER;
  payload: ApiErrorResponse<any>;
};

export type ShowToastAction = {
  type: typeof SHOW_TOAST;
  payload: {
    message: string;
    type: ToastTypes;
  };
};
