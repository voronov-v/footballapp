import {
  FETCH_TEAM_INFO_REQUEST,
  FETCH_TEAM_SCHEDULE_REQUEST,
  FETCH_TEAMS_REQUEST,
} from '@root/general/consts/actions';
import { FetchTeamInfoPayload, FetchTeamSchedulePayload, FetchTeamsPayload } from '@root/redux/actions/types';

export const fetchTeamsRequestAction = (payload: FetchTeamsPayload) => ({
  type: FETCH_TEAMS_REQUEST,
  payload,
});

export const fetchTeamInfoAction = (payload: FetchTeamInfoPayload) => ({
  type: FETCH_TEAM_INFO_REQUEST,
  payload,
});

export const fetchTeamScheduleAction = (payload: FetchTeamSchedulePayload) => ({
  type: FETCH_TEAM_SCHEDULE_REQUEST,
  payload,
});
