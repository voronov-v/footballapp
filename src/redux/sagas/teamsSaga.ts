import { call, put } from 'redux-saga/effects';
import { FetchTeamsAction } from '@root/redux/actions/types';
import { Api } from '@root/services/api';
import { Team } from '@root/general/types';
import {
  CALL_COMMON_ERROR_HANDLER,
  CALL_UNEXPECTED_ERROR_HANDLER,
  FETCH_TEAMS_SUCCEED,
} from '@root/general/consts/actions';
import { ApiOkResponse } from 'apisauce';

export function* fetchTeams(api: Api, action: FetchTeamsAction) {
  const { callback } = action.payload;
  try {
    const response: ApiOkResponse<{ count: number; teams: Array<Team> }> = yield call(api.fetchTeamsRequest);

    if (response.ok) {
      yield put({
        type: FETCH_TEAMS_SUCCEED,
        payload: response.data?.teams ?? [],
      });

      yield callback && call(callback, true);
    }

    if (!response.ok) {
      yield put({ type: CALL_COMMON_ERROR_HANDLER, payload: response });
      yield callback && call(callback, false);
    }
  } catch (error) {
    yield put({ type: CALL_UNEXPECTED_ERROR_HANDLER, payload: error });
    yield callback && call(callback, false);
  }
}
