import { call, put } from 'redux-saga/effects';
import { FetchTeamScheduleAction } from '@root/redux/actions/types';
import { Api } from '@root/services/api';
import { Match } from '@root/general/types';
import { ApiOkResponse } from 'apisauce';
import { CALL_COMMON_ERROR_HANDLER, CALL_UNEXPECTED_ERROR_HANDLER } from '@root/general/consts/actions';

export function* fetchTeamSchedule(api: Api, action: FetchTeamScheduleAction) {
  const { teamId, callback, filter } = action.payload;

  try {
    const response: ApiOkResponse<{ matches: Array<Match> }> = yield call(api.fetchTeamSchedule, teamId, filter);

    if (response.ok) {
      yield callback && call(callback, true, response.data?.matches ?? []);
    }

    if (!response.ok) {
      yield put({ type: CALL_COMMON_ERROR_HANDLER, payload: response });
      yield callback && call(callback, false);
    }
  } catch (error) {
    yield put({ type: CALL_UNEXPECTED_ERROR_HANDLER, payload: error });
    yield callback && call(callback, false);
  }
}
