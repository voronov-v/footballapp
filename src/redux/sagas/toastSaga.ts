import { call } from 'redux-saga/effects';
import Toast from 'react-native-root-toast';
import { ShowToastAction } from '@root/redux/actions/types';
import { ToastTypes } from '@root/general/enums';
import { colors } from '@root/general/colors';

const defaultToastOptions = {
  duration: Toast.durations.LONG,
  position: 100,
  animation: true,
  hideOnPress: true,
  delay: 0,
  visible: true,
  shadow: false,
  opacity: 1,
};

const toastOptions = {
  [ToastTypes.Error]: {
    backgroundColor: colors.red,
  },
  [ToastTypes.Notification]: {
    backgroundColor: colors.celery,
  },
};

export function* showToast(action: ShowToastAction) {
  const { message, type } = action.payload;
  yield call(Toast.show, message, {
    ...defaultToastOptions,
    ...toastOptions[type],
  });
}
