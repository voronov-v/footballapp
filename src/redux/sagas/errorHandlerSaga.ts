import { put } from 'redux-saga/effects';
import { SHOW_TOAST } from '@root/general/consts/actions';
import { strings } from '@root/general/strings';
import { ToastTypes } from '@root/general/enums';
import { CommonErrorHandlerAction } from '@root/redux/actions/types';

const { warnings } = strings;

export function* commonHandler(action: CommonErrorHandlerAction) {
  const response = action.payload;
  if (response.problem === 'NETWORK_ERROR') {
    return yield put({
      type: SHOW_TOAST,
      payload: {
        message: warnings.networkMessage,
        type: ToastTypes.Notification,
      },
    });
  }

  if (response.problem === 'TIMEOUT_ERROR') {
    return yield put({
      type: SHOW_TOAST,
      payload: {
        message: warnings.timeoutMessage,
        type: ToastTypes.Notification,
      },
    });
  }

  yield put({
    type: SHOW_TOAST,
    payload: {
      message: warnings.commonWarning,
      type: ToastTypes.Error,
    },
  });
}

export function* unexpectedErrorHandler() {
  yield put({
    type: SHOW_TOAST,
    payload: {
      message: warnings.commonWarning,
      type: ToastTypes.Error,
    },
  });
}
