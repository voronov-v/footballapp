import { all, takeLatest } from 'redux-saga/effects';
import { create } from '@root/services/api';
import {
  CALL_COMMON_ERROR_HANDLER,
  CALL_UNEXPECTED_ERROR_HANDLER,
  FETCH_TEAM_INFO_REQUEST,
  FETCH_TEAM_SCHEDULE_REQUEST,
  FETCH_TEAMS_REQUEST,
  SHOW_TOAST,
} from '@root/general/consts/actions';
import { fetchTeams } from '@root/redux/sagas/teamsSaga';
import { fetchTeamInfo } from '@root/redux/sagas/teamInfoSaga.ts';
import { fetchTeamSchedule } from '@root/redux/sagas/teamScheduleSaga';
import { showToast } from '@root/redux/sagas/toastSaga';
import { commonHandler, unexpectedErrorHandler } from '@root/redux/sagas/errorHandlerSaga';

const api = create();

export const rootSaga = function* root() {
  yield all([takeLatest(FETCH_TEAMS_REQUEST, fetchTeams, api)]);
  yield all([takeLatest(FETCH_TEAM_INFO_REQUEST, fetchTeamInfo, api)]);
  yield all([takeLatest(FETCH_TEAM_SCHEDULE_REQUEST, fetchTeamSchedule, api)]);
  yield all([takeLatest(SHOW_TOAST, showToast)]);
  yield all([takeLatest(CALL_COMMON_ERROR_HANDLER, commonHandler)]);
  yield all([takeLatest(CALL_UNEXPECTED_ERROR_HANDLER, unexpectedErrorHandler)]);
};
