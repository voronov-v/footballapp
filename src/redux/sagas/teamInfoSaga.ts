import { call, put } from 'redux-saga/effects';
import { FetchTeamInfoAction } from '@root/redux/actions/types';
import { Api } from '@root/services/api';
import { Team } from '@root/general/types';
import { ApiOkResponse } from 'apisauce';
import { CALL_COMMON_ERROR_HANDLER, CALL_UNEXPECTED_ERROR_HANDLER } from '@root/general/consts/actions';

export function* fetchTeamInfo(api: Api, action: FetchTeamInfoAction) {
  const { teamId, callback } = action.payload;

  try {
    const response: ApiOkResponse<Team> = yield call(api.fetchTeamInfoRequest, teamId);

    if (response.ok) {
      yield callback && call(callback, true, response.data);
    }

    if (!response.ok) {
      yield put({ type: CALL_COMMON_ERROR_HANDLER, payload: response });
      yield callback && call(callback, false);
    }
  } catch (error) {
    yield put({ type: CALL_UNEXPECTED_ERROR_HANDLER, payload: error });
    yield callback && call(callback, false);
  }
}
