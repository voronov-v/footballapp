import { TeamsReducer } from '@root/redux/reducers/teamsReducer';

export type IActionType = {
  payload?: any;
  type: string;
};

export type IState = {
  readonly teams: TeamsReducer;
};
