import React, { useEffect, useState } from 'react';
import { View, FlatList, SafeAreaView } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { fetchTeamsRequestAction } from '@root/redux/actions';
import { getTeams } from '@root/selectors';
import { keyExtractor } from '@root/utils/keyExtractor';
import { CustomLoader } from '@root/components/CustomLoader';
import { TeamCard } from '@root/screens/TeamsScreen/components/TeamCard';
import { RootStackScreenProps } from '@root/navigators/types';
import { TEAM_INFO_SCREEN } from '@root/general/consts/screenNames';
import { styles } from './styles';
import { ListEmptyComponent } from '@root/components/ListEmptyComponent';

export const TeamsScreen = (props: RootStackScreenProps<'TEAMS_SCREEN'>) => {
  const { navigation } = props;

  const dispatch = useDispatch();

  const { teamsList } = useSelector(getTeams);

  const [isLoading, setIsLoading] = useState(false);
  const [refreshing, setRefreshing] = useState(false);

  useEffect(() => {
    const callback = () => {
      setIsLoading(false);
    };

    setIsLoading(true);
    dispatch(fetchTeamsRequestAction({ callback }));
  }, [dispatch]);

  const handleRefresh = () => {
    setRefreshing(true);
    dispatch(fetchTeamsRequestAction({ callback: () => setRefreshing(false) }));
  };

  const goToTeamInfo = (teamId: number) => () => navigation.navigate(TEAM_INFO_SCREEN, { teamId });

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.container}>
        {isLoading ? (
          <CustomLoader />
        ) : (
          <FlatList
            data={teamsList}
            renderItem={({ item }) => <TeamCard team={item} onPress={goToTeamInfo(item.id)} />}
            keyExtractor={keyExtractor}
            refreshing={refreshing}
            onRefresh={handleRefresh}
            ListEmptyComponent={ListEmptyComponent}
          />
        )}
      </View>
    </SafeAreaView>
  );
};
