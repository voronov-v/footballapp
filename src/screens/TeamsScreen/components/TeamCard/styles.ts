import { StyleSheet } from 'react-native';
import { colors } from '@root/general/colors';

export const styles = StyleSheet.create({
  teamItemContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingVertical: 15,
    paddingHorizontal: 15,
    borderBottomWidth: 1,
    borderBottomColor: colors.redOpacity1,
  },
  teamDescContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  teamLogoContainer: {
    width: 45,
  },
  teamNameText: {
    marginLeft: 10,
    fontSize: 15,
    color: colors.black,
  },
});
