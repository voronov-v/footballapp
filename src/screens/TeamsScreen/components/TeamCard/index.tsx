import React from 'react';
import { Text, TouchableOpacity, View } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { colors } from '@root/general/colors';
import { TeamCardProps } from './types';
import { styles } from './styles';
import { TeamLogo } from '@root/components/TeamLogo';

export const TeamCard = (props: TeamCardProps) => {
  const { team, onPress } = props;
  const { name, crestUrl, tla } = team;

  return (
    <TouchableOpacity style={styles.teamItemContainer} onPress={onPress}>
      <View style={styles.teamDescContainer}>
        <TeamLogo uri={crestUrl} containerStyle={styles.teamLogoContainer} />
        <Text style={styles.teamNameText}>{`${name} (${tla})`}</Text>
      </View>
      <Icon name='arrow-right' size={20} color={colors.black} />
    </TouchableOpacity>
  );
};
