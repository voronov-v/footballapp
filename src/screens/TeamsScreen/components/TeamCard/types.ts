import { Team } from '@root/general/types';

export type TeamCardProps = {
  team: Team;
  onPress: () => void;
};
