import { StyleSheet } from 'react-native';
import { colors } from '@root/general/colors';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
    backgroundColor: colors.lightGrey,
  },
  halfContainer: {
    flex: 1,
  },
  teamLogoContainer: {
    alignItems: 'center',
    paddingVertical: 15,
  },
  listContainer: {
    flex: 1,
    width: '100%',
  },
  listHeaderContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: colors.grey,
    justifyContent: 'center',
    paddingVertical: 10,
  },
  listHeaderText: {
    fontSize: 16,
    fontWeight: '700',
    marginRight: 6,
  },
});
