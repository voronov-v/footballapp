import { Match } from '@root/general/types';

export type TeamScheduleProps = {
  teamSchedule: Array<Match>;
  isLoading: boolean;
};
