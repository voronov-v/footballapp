import React from 'react';
import { FlatList, View } from 'react-native';
import { keyExtractor } from '@root/utils/keyExtractor';
import { strings } from '@root/general/strings';
import { TeamScheduleProps } from './types';
import { ListEmptyComponent } from '@root/components/ListEmptyComponent';
import { styles } from '@root/screens/TeamInfoScreen/styles';
import { MatchCard } from '@root/screens/TeamInfoScreen/components/MatchCard';
import { CustomLoader } from '@root/components/CustomLoader';
import { ListHeader } from '@root/screens/TeamInfoScreen/components/ListHeader';

export const TeamSchedule = (props: TeamScheduleProps) => {
  const { teamSchedule, isLoading } = props;

  return (
    <View style={styles.halfContainer}>
      {isLoading ? (
        <CustomLoader />
      ) : (
        <View style={styles.listContainer}>
          <ListHeader title={strings.teamInfoScreen.upcomingMatches} iconName={'calendar'} />
          <FlatList
            data={teamSchedule}
            renderItem={({ item }) => <MatchCard match={item} />}
            keyExtractor={keyExtractor}
            ListEmptyComponent={ListEmptyComponent}
          />
        </View>
      )}
    </View>
  );
};
