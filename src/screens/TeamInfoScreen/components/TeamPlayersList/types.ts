import { Player } from '@root/general/types';

export type TeamPlayersListProps = {
  teamPlayers?: Array<Player>;
  isLoading: boolean;
};
