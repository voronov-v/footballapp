import React from 'react';
import { FlatList, View } from 'react-native';
import { keyExtractor } from '@root/utils/keyExtractor';
import { PlayerCard } from '@root/screens/TeamInfoScreen/components/PlayerCard';
import { strings } from '@root/general/strings';
import { TeamPlayersListProps } from './types';
import { ListEmptyComponent } from '@root/components/ListEmptyComponent';
import { styles } from '@root/screens/TeamInfoScreen/styles';
import { CustomLoader } from '@root/components/CustomLoader';
import { ListHeader } from '@root/screens/TeamInfoScreen/components/ListHeader';

export const TeamPlayersList = (props: TeamPlayersListProps) => {
  const { teamPlayers = [], isLoading } = props;

  return (
    <View style={styles.halfContainer}>
      {isLoading ? (
        <CustomLoader />
      ) : (
        <View style={styles.listContainer}>
          <ListHeader title={strings.teamInfoScreen.players} iconName={'futbol-o'} />
          <FlatList
            data={teamPlayers}
            renderItem={({ item }) => <PlayerCard player={item} />}
            keyExtractor={keyExtractor}
            ListEmptyComponent={ListEmptyComponent}
          />
        </View>
      )}
    </View>
  );
};
