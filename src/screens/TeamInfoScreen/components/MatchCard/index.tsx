import React from 'react';
import { Text, View } from 'react-native';
import { MatchItemProps } from './types';
import { styles } from './styles';
import moment from 'moment';
import { colors } from '@root/general/colors';
import Icon from 'react-native-vector-icons/FontAwesome';

export const MatchCard = (props: MatchItemProps) => {
  const { match } = props;

  const homeTeam = match?.homeTeam?.name ?? '';
  const awayTeam = match?.awayTeam?.name ?? '';
  const competitionName = match?.competition?.name ?? '';
  const date = moment(match?.utcDate ?? '').format('DD.MM');

  return (
    <View style={styles.matchItemContainer}>
      <View style={styles.matchTeamsContainer}>
        <Text style={styles.matchDateText}>{date}</Text>
        <View>
          <View style={styles.homeTeamContainer}>
            <Text>{homeTeam} </Text>
            <Icon name='home' size={16} color={colors.black} />
          </View>
          <Text>{awayTeam}</Text>
        </View>
      </View>
      <Text>{competitionName}</Text>
    </View>
  );
};
