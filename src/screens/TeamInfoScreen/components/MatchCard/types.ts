import { Match } from '@root/general/types';

export type MatchItemProps = {
  match: Match;
};
