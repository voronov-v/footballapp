import { StyleSheet } from 'react-native';
import { colors } from '@root/general/colors';

export const styles = StyleSheet.create({
  matchItemContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingVertical: 15,
    paddingHorizontal: 15,
    borderBottomWidth: 1,
    borderBottomColor: colors.redOpacity1,
  },
  matchTeamsContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  matchDateText: {
    fontSize: 16,
    fontWeight: 'bold',
    marginRight: 6,
  },
  homeTeamContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
});
