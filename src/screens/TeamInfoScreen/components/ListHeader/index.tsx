import React from 'react';
import { View, Text } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { colors } from '@root/general/colors';
import { ListHeaderProps } from '@root/screens/TeamInfoScreen/components/ListHeader/types';
import { styles } from '@root/screens/TeamInfoScreen/styles';

export const ListHeader = (props: ListHeaderProps) => {
  const { title, iconName } = props;
  return (
    <View style={styles.listHeaderContainer}>
      <Text style={styles.listHeaderText}>{title}</Text>
      <Icon name={iconName} size={20} color={colors.black} />
    </View>
  );
};
