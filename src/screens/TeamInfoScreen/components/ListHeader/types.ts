export type ListHeaderProps = {
  title: string;
  iconName: string;
};
