import { StyleSheet } from 'react-native';
import { colors } from '@root/general/colors';

export const styles = StyleSheet.create({
  playerItemContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingVertical: 15,
    paddingHorizontal: 15,
    borderBottomWidth: 1,
    borderBottomColor: colors.redOpacity1,
  },
  playerDescContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  playerNameText: {
    marginLeft: 6,
    fontSize: 16,
    fontWeight: 'bold',
  },
  playerPositionText: {
    textAlign: 'right',
    fontSize: 14,
    fontWeight: '500',
  },
});
