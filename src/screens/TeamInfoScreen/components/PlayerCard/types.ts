import { Player } from '@root/general/types';

export type PlayerCardProps = {
  player: Player;
};
