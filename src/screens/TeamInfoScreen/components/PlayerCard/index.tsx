import React from 'react';
import { Text, View } from 'react-native';
import { PlayerCardProps } from './types';
import { styles } from './styles';

export const PlayerCard = (props: PlayerCardProps) => {
  const { player } = props;

  const name = player?.name ?? '';
  const position = player?.position ?? '';
  const country = player?.countryOfBirth ?? '';

  return (
    <View style={styles.playerItemContainer}>
      <View style={styles.playerDescContainer}>
        <Text>{country}</Text>
        <Text style={styles.playerNameText}>{name}</Text>
      </View>
      <Text style={styles.playerPositionText}>{position}</Text>
    </View>
  );
};
