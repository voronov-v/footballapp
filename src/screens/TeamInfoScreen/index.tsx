import React, { useEffect, useState } from 'react';
import { SafeAreaView, View } from 'react-native';
import { RootStackScreenProps } from '@root/navigators/types';
import { useDispatch } from 'react-redux';
import { fetchTeamInfoAction, fetchTeamScheduleAction } from '@root/redux/actions';
import { Match, Team } from '@root/general/types';
import { TeamPlayersList } from './components/TeamPlayersList';
import { TeamSchedule } from './components/TeamSchedule';
import { TeamLogo } from '@root/components/TeamLogo';
import moment from 'moment';
import { styles } from './styles';

export const TeamInfoScreen = (props: RootStackScreenProps<'TEAM_INFO_SCREEN'>) => {
  const { route, navigation } = props;
  const { teamId } = route.params;

  const dispatch = useDispatch();

  const [isLoading, setLoading] = useState(true);
  const [isScheduleLoading, setScheduleLoading] = useState(true);
  const [teamInfo, setTeamInfo] = useState<null | Team>(null);
  const [teamSchedule, setTeamSchedule] = useState<Array<Match>>([]);

  useEffect(() => {
    const callback = (isSuccess: boolean, teamInfo?: Team) => {
      isSuccess && teamInfo && setTeamInfo(teamInfo);
      setLoading(false);
    };

    setLoading(true);
    dispatch(fetchTeamInfoAction({ teamId, callback }));
  }, [dispatch, teamId]);

  useEffect(() => {
    const callback = (isSuccess: boolean, teamSchedule?: Array<Match>) => {
      isSuccess && teamSchedule && setTeamSchedule(teamSchedule);
      setScheduleLoading(false);
    };

    setScheduleLoading(true);
    dispatch(
      fetchTeamScheduleAction({
        teamId,
        callback,
        filter: { dateFrom: moment(new Date()).format('YYYY-MM-DD'), limit: 30 },
      }),
    );
  }, [dispatch, teamId]);

  useEffect(() => {
    teamInfo && navigation.setOptions({ title: teamInfo.name });
  }, [navigation, teamInfo]);

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.container}>
        <TeamLogo uri={teamInfo?.crestUrl} size={60} containerStyle={styles.teamLogoContainer} />
        <TeamPlayersList teamPlayers={teamInfo?.squad} isLoading={isLoading} />
        <TeamSchedule teamSchedule={teamSchedule} isLoading={isScheduleLoading} />
      </View>
    </SafeAreaView>
  );
};
