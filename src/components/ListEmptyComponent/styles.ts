import { StyleSheet } from 'react-native';
import { colors } from '@root/general/colors';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: colors.lightGrey,
    paddingTop: 20,
  },
});
