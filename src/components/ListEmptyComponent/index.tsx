import React from 'react';
import { View, Text } from 'react-native';
import { strings } from '@root/general/strings';
import { styles } from './styles';

export const ListEmptyComponent = () => {
  return (
    <View style={styles.container}>
      <Text>{strings.warnings.noDataMessage}</Text>
    </View>
  );
};
