import React from 'react';
import { ActivityIndicator, View } from 'react-native';
import { colors } from '@root/general/colors';
import { styles } from './styles';

export const CustomLoader = () => {
  return (
    <View style={styles.container}>
      <ActivityIndicator size='large' color={colors.red} />
    </View>
  );
};
