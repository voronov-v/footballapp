import React from 'react';
import { SvgUri } from 'react-native-svg';
import { Image, View } from 'react-native';
import { TeamLogoProps } from './types';
import { styles } from './styles';

export const TeamLogo = (props: TeamLogoProps) => {
  const { uri, size = 40, containerStyle } = props;

  const imageProps = { width: size, height: size };

  const getLogo = () => {
    if (uri) {
      if (uri.slice(-4) === '.svg') {
        return <SvgUri {...imageProps} uri={uri} />;
      } else {
        return <Image style={[styles.teamLogo, imageProps]} source={{ uri }} />;
      }
    }

    return (
      <Image
        style={[styles.teamLogo, imageProps]}
        source={{
          uri: 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRKN9aKcsHBELbc0X5Js73LwozUM-BS17FhOw&usqp=CAU',
        }}
      />
    );
  };

  return <View style={[styles.teamLogoContainer, containerStyle]}>{getLogo()}</View>;
};
