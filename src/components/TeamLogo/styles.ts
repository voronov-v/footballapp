import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  teamLogoContainer: {
    minHeight: 40,
  },
  teamLogo: {
    resizeMode: 'contain',
    width: 40,
    height: 40,
  },
});
