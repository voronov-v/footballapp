import { StyleProp, ViewStyle } from 'react-native';

export type TeamLogoProps = {
  uri?: string | null;
  size?: number;
  containerStyle?: StyleProp<ViewStyle>;
};
