import { IState } from '@root/redux/types';

export const getTeams = (state: IState) => state.teams;
