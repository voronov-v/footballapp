import React, { useEffect } from 'react';
import { Platform, StatusBar } from 'react-native';
import { Provider } from 'react-redux';
import { store } from '@root/redux/store';
import SplashScreen from 'react-native-splash-screen';
import { RootSiblingParent } from 'react-native-root-siblings';
import { RootNavigator } from '@root/navigators';

export const App = () => {
  useEffect(() => {
    SplashScreen.hide();
  }, []);

  const ToastWrapper = Platform.OS === 'ios' ? React.Fragment : RootSiblingParent;

  return (
    <Provider store={store}>
      <StatusBar barStyle='dark-content' />
      <ToastWrapper>
        <RootNavigator />
      </ToastWrapper>
    </Provider>
  );
};
