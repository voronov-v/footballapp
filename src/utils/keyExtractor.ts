export type Item = {
  [key: string]: any;
};

export const keyExtractor = (item: Item) => `${item.id}`;
