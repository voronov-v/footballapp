export const colors = {
  white: '#ffffff',
  black: '#000000',
  lightGrey: '#FBFBFB',
  grey: '#B3BAC1',
  celery: '#9CBF5E',
  red: '#D0392A',
  redOpacity1: 'hsla(5, 66%, 49%, 0.1)',
};
