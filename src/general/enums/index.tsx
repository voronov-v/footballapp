export enum ToastTypes {
  Error = 'Error',
  Notification = 'Notification',
}
