export const API_TOKEN = 'fa4d34687e234bcbaedc449ae1eecebf';

export const GENERAL_HEADERS: { [key: string]: string } = {
  'content-type': 'application/json',
  'X-Auth-Token': API_TOKEN,
};

export const BASE_URL = 'https://api.football-data.org/v2';

export const TIMEOUT = 3000;
