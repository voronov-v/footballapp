export const strings = {
  teamsScreen: {
    screenTitle: 'Teams',
  },
  teamInfoScreen: {
    screenTitle: '',
    players: 'Players',
    upcomingMatches: 'Upcoming Matches',
  },
  warnings: {
    timeoutMessage: 'Looks like the server is taking too long to respond',
    networkMessage: 'The network connection is lost',
    commonWarning: 'Something went wrong, please contact support',
    noDataMessage: 'Sorry, no data found',
  },
};
