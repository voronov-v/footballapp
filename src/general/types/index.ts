export type Team = {
  id: number;
  address: string;
  area: Area;
  clubColors: string;
  crestUrl?: string | null;
  email: string;
  founded: number;
  lastUpdated: string;
  name: string;
  phone: string;
  shortName: string;
  tla: string;
  venue: string;
  website: string;
  squad?: Array<Player>;
  activeCompetitions?: Array<TeamCompetition>;
};

export type Area = {
  id: number;
  name: string;
};

export type Player = {
  countryOfBirth: string;
  dateOfBirth: string;
  id: number;
  name: string;
  nationality: string;
  position: string;
  role: string;
  shirtNumber: null;
};

export type Competition = {
  id: number;
  name: string;
  area: Area;
};

export type TeamCompetition = Competition & {
  code: string;
  lastUpdated: string;
  plan: string;
};

export type Match = {
  id: number;
  homeTeam: { id: number; name: string };
  awayTeam: { id: number; name: string };
  competition: Competition;
  utcDate: string;
  // todo match type
};

export type ScheduleFilter = {
  dateFrom?: string;
  limit?: number;
};
