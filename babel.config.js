module.exports = {
  plugins: [
    [
      'module-resolver',
      {
        cwd: 'babelrc',
        root: ['./src'],
        alias: {
          '@root': './src',
        },
      },
    ],
  ],
  presets: ['module:metro-react-native-babel-preset'],
};
